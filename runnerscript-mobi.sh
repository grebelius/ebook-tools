#!/bin/bash

echo "first: 1 | last: 1000";
./ebookTools -m -f 1 -l 1000
echo "1-1000 done";

for i in `seq 1 999`;
do
        let "first=`expr $i \* 1000 + 1`";
        let "last=`expr $i \* 1000 + 1000`";
        echo "first: $first | last: $last";
        ./ebookTools -m -f $first -l $last;
        echo "$first-$last done";
done
