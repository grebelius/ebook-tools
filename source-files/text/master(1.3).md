---
title:
- type: main
  text: "One Trillion Digits of Pi: Volume __title-number__ of 1,000,000"
creator:
- role: author
  text: Lennart Grebelius
  file-as: Grebelius, Lennart
identifier:
- scheme: DOI
  text: __doi__
date: __date__
lang: en-US
page-progression-direction: ltr
publisher:  Sätila Publishing
rights: Copyleft, no rights reserved
...

# Foreword

The number Pi is the ratio of a circle’s circumference to its diameter, frequently approximated as 3.14. It has been represented by the Greek letter π since the mid-18th century. π cannot be expressed exactly. Its decimal representation never ends and never settles into a permanent repeating pattern. The digits appear to be randomly distributed; however, to date, no proof of this has been discovered. The properties of π has fascinated expert and amateur mathematicians since antiquity.

In this work, π has been calculated to one trillion decimal places, which must be regarded as a precise approximation. I calculated the first 10 million digits on a laptop in my studio. Shigeru Kondo and Alexander Yee generously provided me with the remaining 999,990 million.

## {-}

>_“The mathematician’s patterns, like the painter’s or the poet’s, must be beautiful; the ideas, like the colours or the words, must fit together in a harmonious way. Beauty is the first test: there is no permanent place in this world for ugly mathematics.”_\
>_— G. H. Hardy (1877–1947)_

# Digits __title-number-minus-one__,000,001 to __title-number-minus-one__,100,000 {.pi}
__pi-1__

# Digits __title-number-minus-one__,100,001 to __title-number-minus-one__,200,000 {.pi}
__pi-2__

# Digits __title-number-minus-one__,200,001 to __title-number-minus-one__,300,000 {.pi}
__pi-3__

# Digits __title-number-minus-one__,300,001 to __title-number-minus-one__,400,000 {.pi}
__pi-4__

# Digits __title-number-minus-one__,400,001 to __title-number-minus-one__,500,000 {.pi}
__pi-5__

# Digits __title-number-minus-one__,500,001 to __title-number-minus-one__,600,000 {.pi}
__pi-6__

# Digits __title-number-minus-one__,600,001 to __title-number-minus-one__,700,000 {.pi}
__pi-7__

# Digits __title-number-minus-one__,700,001 to __title-number-minus-one__,800,000 {.pi}
__pi-8__

# Digits __title-number-minus-one__,800,001 to __title-number-minus-one__,900,000 {.pi}
__pi-9__

# Digits __title-number-minus-one__,900,001 to __title-number__,000,000 {.pi}
__pi-10__

# Afterword

>_“Ten decimal places of π are sufficient to give the circumference of the earth to a fraction of an inch, and thirty decimal places would give the circumference of the visible universe to a quantity imperceptible to the most powerful microscope.”_\
>_— Simon Newcomb (1835–1909)_

## {-}

One Trillion Digits of Pi, Volume __title-number__ (The digital edition) has also been printed as a pocket edition limited to only one copy. I reserve no copyright though. Anyone who wishes to produce millions or billions of digits of Pi is free to copy this work.