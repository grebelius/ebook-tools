--
**Artist decided to ditch epub/mobi because of the inherent difficulty of controlling page breaks, fonts sizes etc. Moving on to [grebelius/pdf-generators](https://gitlab.com/grebelius/pdf-generators)**.
--

# Tools for generating millions of ebooks (for an art project)

## About the artist
[Lennart Grebelius](https://satilaholding.se/for-the-press-about-lennart-grebelius/) is a Swedish artist. Common to most of Lennart’s work is an interest in existential questions and grandiose repetition. “His best work is distinguished by an unquenchable will to structure the apparently infinite,” wrote the critic Mark Isitt in the Gothenburg Post.

Much of Lennart’s art combines an interest in books with an interest in the big questions: life, death, time and chance. Beautiful mathematics is also a fundamental theme. It’s no coincidence that he had a Fibonacci series painted on the wall of one of his properties in central Gothenburg.

## About the ebooks I need to create

At the moment I'm working on the project *One Trillion Digits of Pi* for Mr. Grebelius. From the preface of the book:

>In this work by Lennart Grebelius, Pi has been calculated to one trillion decimal places. It spans one million total volumes, each containing one million digits. It must be underlined though, that all these volumes combined are still just an approximation.

## The challenge

I need to generate one million ebooks, each containing the correct part of the first trillion digits of Pi. For this I will need scripts. I will try to create them here.

## The plan

- [x] Learn Git and GitHub.
- [x] Program for generating 1 million epub covers and 1 million mobi covers.
- [x] Program for converting pi txt files to [pandoc](https://github.com/jgm/pandoc) md documents.
- [x] Program for adding yaml-metadata to the [pandoc](https://github.com/jgm/pandoc) md documents.
- [x] Create CSS for epub.
- [x] Create CSS for mobi.
- [x] Program for creating epub files and mobi files with [pandoc](https://github.com/jgm/pandoc) and KindleGen

## Terminal commands I need to remember

### Create epub with [pandoc](https://github.com/jgm/pandoc)
#### First iteration
```sh
time pandoc -o test.epub pi-1-test-spaces.md --toc --toc-depth=1 --epub-chapter-level=2 --epub-cover-image=cover-dummy.jpg --epub-stylesheet=epub.css --epub-embed-font=fonts/BemboStd*.otf
```

### Create epub with [pandoc](https://github.com/jgm/pandoc)
#### Second iteration with custom css and yaml-metadata in the md file
```sh
time pandoc -o test.epub pi-1-test-spaces.md --epub-chapter-level=2 --toc --toc-depth=1 --epub-cover-image=cover-dummy.jpg --epub-stylesheet=epub.css --epub-embed-font=fonts/BemboStd*.otf
```

### Create epub with [pandoc](https://github.com/jgm/pandoc)
#### Third iteration without TOC
```sh
time\
pandoc -o test.epub\
pi-1-test-spaces.md\
--epub-chapter-level=2\
--toc-depth=1\
--epub-cover-image=cover-dummy.jpg\
--epub-stylesheet=epub.css\
--epub-embed-font=fonts/BemboStd*.otf
```

### Obfuscate fonts
```sh
./glyphIgo.py obfuscate -f BemboStd-Italic.otf -i "isbn-1" -o obf.BemboStd-Italic.otf
```

### Check epub validity
```sh
time epubcheck test.epub
```

### Convert epub to mobi with kindlegen
```sh
time kindlegen test.epub
```